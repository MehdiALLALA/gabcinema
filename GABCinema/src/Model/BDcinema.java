package Model;

import java.sql.DriverManager;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class BDcinema {
	Connection con = null;
	ResultSet rs = null ;
	Statement stmt = null ;
	public BDcinema() { 
 }
	public boolean driver()
	{ try{ 
	   Class.forName("com.mysql.jdbc.Driver");
	   return true ;      
	   }
	 catch(Exception e){
	   System.out.println("Erreur lors du chargement du pilote :"+ e.getMessage());
	   return false ;
	   } 
	}
	public boolean OpenConnexion()
	{ try{
	 String  url = "jdbc:mysql://localhost/cinema";
	 con = (Connection) DriverManager.getConnection(url,"root","");
	 
	 return true ;
	 }
	 catch (Exception e)
	{
	  System.out.println("Echec de l'ouverture de la connexion :"+ e.getMessage());
	  return false ;
	 }
	}
	public boolean closeConnexion()
	{ try{  con.close();
	 return true ;
	 } 
	  catch (Exception e){
	  System.out.println("Echec de la fermeture de la connexion :"+ e.getMessage());
	  return false ;
	 } 
	} 
	public ResultSet selectExec(String sql){
		try
		{  stmt = (Statement) con.createStatement();
		 rs = stmt.executeQuery(sql);
		 }  catch(Exception e){
		 System.out.println("Echec de l'ex�cution de la requ�te sql :"+e.getMessage());
		 }   
		return rs ; 
		 }
	public int updateExec(String sql)
	{ 
	 int i = 0 ;
	 try{ 
	 con.setAutoCommit(false);
	 stmt = (Statement) con.createStatement() ;
	 i = stmt.executeUpdate(sql) ;  
	con.commit();
	 } 
	 catch(Exception e)
	{
	 System.out.println("Echec de l'ex�cution de la requ�te sql :"+e.getMessage());  
	}  
	 return i ; 
	}
	public boolean closeResultSet(){
		 try{  rs.close(); 
			return true ;  }
		  catch (Exception e){
		  System.out.println("Echec de la fermeture de l'objet ResultSet :"+ e.getMessage());
		  return false ;
		  } 
		 }
	public boolean closeStatement()
	{  try{  stmt.close();  
	   return true ; 
	 } 
	  catch (Exception e){   
		  System.out.println("Echec de la fermeture de l'objet Statement :"+ e.getMessage());   
		  return false ; 
	 } 
	} 
	
	public static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }

}
