package Model;

import java.sql.Date;
import javafx.collections.ObservableList;
import javafx.scene.image.ImageView;

public class FilmDetails {
    private ImageView image;
    private int id_film;
    private  String titre;
    private  String resume;
    private  String image_path;
    private  Date date;
    
    public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public void setImage_path(String image_path) {
		this.image_path = image_path;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public FilmDetails(ImageView image,int id_film ,String titre, String resume, String image_path, Date date)
    {
       this.image = image;
     //  System.out.println("image obvject: " + this.image);
       this.id_film = id_film;
       this.titre = titre;
       this.resume = resume;
       this.image_path =  image_path;
       this.date =date;

    
    }

	public FilmDetails(ObservableList<FilmDetails> selectedItems) {
		// TODO Auto-generated constructor stub
	}

	public ImageView getImage() {
		return image;
	}

	public void setImage(ImageView image) {
		this.image = image;
	}

	public String getTitre() {
		return titre;
	}

	public String getResume() {
		return resume;
	}

	public String getImage_path() {
		return image_path;
	}

	public Date getDate() {
		return date;
	}

	public int getId_film() {
		return id_film;
	}

	public void setId_film(int id_film) {
		this.id_film = id_film;
	}
	
}
