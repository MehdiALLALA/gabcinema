package Model;

import java.sql.Date;

public class seanceDetails {
	 private int id_seance,num_salle,id_film,nbre_place,nbre_place_dispo;
	 private String titre;
	 private Date date_diffusion;
	 private String heure_diffusion;
	 private float prix;
	public int getId_seance() {
		return id_seance;
	}
	public void setId_seance(int id_seance) {
		this.id_seance = id_seance;
	}
	public int getNum_salle() {
		return num_salle;
	}
	public void setNum_salle(int num_salle) {
		this.num_salle = num_salle;
	}
	public int getId_film() {
		return id_film;
	}
	public void setId_film(int id_film) {
		this.id_film = id_film;
	}
	public int getNbre_place() {
		return nbre_place;
	}
	public void setNbre_place(int nbre_place) {
		this.nbre_place = nbre_place;
	}
	public int getNbre_place_dispo() {
		return nbre_place_dispo;
	}
	public void setNbre_place_dispo(int nbre_place_dispo) {
		this.nbre_place_dispo = nbre_place_dispo;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public Date getDate_diffusion() {
		return date_diffusion;
	}
	public void setDate_diffusion(Date date_diffusion) {
		this.date_diffusion = date_diffusion;
	}
	public String getHeure_diffusion() {
		return heure_diffusion;
	}
	public void setHeure_diffusion(String heure_diffusion) {
		this.heure_diffusion = heure_diffusion;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public seanceDetails(int id_seance, int num_salle, int id_film, int nbre_place, int nbre_place_dispo, String titre,
			Date date_diffusion, String heure_diffusion, float prix) {
		
		this.id_seance = id_seance;
		this.num_salle = num_salle;
		this.id_film = id_film;
		this.nbre_place = nbre_place;
		this.nbre_place_dispo = nbre_place_dispo;
		this.titre = titre;
		this.date_diffusion = date_diffusion;
		this.heure_diffusion = heure_diffusion;
		this.prix = prix;
	
	}
	
}
