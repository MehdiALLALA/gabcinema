package Model;

import java.sql.Date;
import java.sql.Time;

import javafx.scene.image.ImageView;

public class OfferDetails {
    private ImageView image;
    private int id_seance;
    private String titre;
    private String description;
    private Date date;
    private Time time;
    private int salle;
	private float prix;
	private int place;
	public OfferDetails(ImageView image, int id_seance,String titre, String description, Date date, Time time, int salle,
			float prix, int place) {
		super();
		this.image = image;
		this.id_seance = id_seance;
		this.titre = titre;
		this.description = description;
		this.date = date;
		this.time = time;
		this.salle = salle;
		this.prix = prix;
		this.place = place;
	}
	public ImageView getImage() {
		return image;
	}
	public void setImage(ImageView image) {
		this.image = image;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public int getSalle() {
		return salle;
	}
	public void setSalle(int salle) {
		this.salle = salle;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public int getPlace() {
		return place;
	}
	public void setPlace(int place) {
		this.place = place;
	}
	public int getId_seance() {
		return id_seance;
	}
	public void setId_seance(int id_seance) {
		this.id_seance = id_seance;
	}
	
   
}
