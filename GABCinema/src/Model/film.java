package Model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;


public class film extends BDcinema {
private int id_film;
private String titre,resume,image;
private Date date;
public film(int id_film, String titre, String resume, String image, Date date) {
	super();
	this.id_film = id_film;
	this.titre = titre;
	this.resume = resume;
	this.image = image;
	this.date = date;
}
public film() {
	// TODO Auto-generated constructor stub
}
public int getId_film() {
	return id_film;
}
public void setId_film(int id_film) {
	this.id_film = id_film;
}
public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}
public String getResume() {
	return resume;
}
public void setResume(String resume) {
	this.resume = resume;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
@Override
public String toString() {
	return "film [id_film=" + id_film + ", titre=" + titre + ", resume=" + resume + ", image=" + image + ", date="
			+ date + "]";
}


public void addFilm(film f)
	{	

	String ch;
	 driver();
	 OpenConnexion();
	 ch ="INSERT INTO `film`( `titre`, `resume`, `image`, `date`) VALUES ('"+f.titre.toUpperCase() +"','"+ f.resume+"','"+f.image+"','"+f.date+"')";
	
	 updateExec(ch);
	 closeConnexion();
	 }


public ResultSet listFilm()
{ 
	 ResultSet rs=null;
	 String ch;
	 driver();
	 OpenConnexion();
	 ch="SELECT * FROM film";
	 rs=selectExec(ch);
	 closeConnexion();
	 return(rs);
}

public void updateFilm(int id,film f){
	driver();
	OpenConnexion();
	updateExec("UPDATE `film` SET `titre`='"+f.getTitre().toUpperCase()+"',`resume`='"+f.getResume()+"',`image`='"+f.getImage()+"',`date`='"+f.getDate()+"' WHERE id_film = "+id);
	closeConnexion();
	}
public void deleteFilm(String ch){
	driver();
	OpenConnexion();
	updateExec("DELETE FROM `film` WHERE titre = '"+ch+"'");
	closeConnexion();
	}

public boolean filmExist(String ch1) throws SQLException
{
	driver();
	OpenConnexion();
String c=	"SELECT `titre` FROM `film` WHERE `titre` = '"+ch1.toUpperCase()+"'";
rs=selectExec(c);

if (rs.next()) return true;

return false;
	}

public boolean filmExist(String ch1, int id) throws SQLException
{
	driver();
	OpenConnexion();
String c=	"SELECT `titre` FROM `film` WHERE `titre` = '"+ch1.toUpperCase()+"' AND id_film <> "+id;
rs=selectExec(c);

if (rs.next()) return true;

return false;
	}


}
