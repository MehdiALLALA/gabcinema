package Model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

public class seance extends BDcinema {
int id_seance,num_salle,id_film,nbre_place,nbre_place_dispo;
Date date_diffusion;
Time heure_diffsuion;
float prix;
public seance(int id_seance, int num_salle, int id_film, int nbre_place,int nbre_place_dispo, Date date_diffusion, Time heure_diffsuion,float prix) {
	super();
	this.id_seance = id_seance;
	this.num_salle = num_salle;
	this.id_film = id_film;
	this.nbre_place = nbre_place;
	this.nbre_place_dispo = nbre_place_dispo;
	this.date_diffusion = date_diffusion;
	this.heure_diffsuion = heure_diffsuion;
	this.prix = prix;;
}

public seance() {
	// TODO Auto-generated constructor stub
}

public int getId_seance() {
	return id_seance;
}
public void setId_seance(int id_seance) {
	this.id_seance = id_seance;
}
public int getNum_salle() {
	return num_salle;
}
public void setNum_salle(int num_salle) {
	this.num_salle = num_salle;
}
public int getId_film() {
	return id_film;
}
public void setId_film(int id_film) {
	this.id_film = id_film;
}

public int getNbre_place() {
	return nbre_place;
}
public void setNbre_place(int nbre_place) {
	this.nbre_place = nbre_place;
}
public int getNbre_place_dispo() {
	return nbre_place_dispo;
}
public void setNbre_place_dispo(int nbre_place_dispo) {
	this.nbre_place_dispo = nbre_place_dispo;
}




@Override
public String toString() {
	return "seance [id_seance=" + id_seance + ", num_salle=" + num_salle + ", id_film=" + id_film + ", nbre_place="
			+ nbre_place + ", nbre_place_dispo=" + nbre_place_dispo + ", date_diffusion=" + date_diffusion
			+ ", heure_diffsuion=" + heure_diffsuion + ", prix=" + prix + "]";
}

public Date getDate_diffusion() {
	return date_diffusion;
}
public void setDate_diffusion(Date date_diffusion) {
	this.date_diffusion = date_diffusion;
}
public Time getHeure_diffsuion() {
	return heure_diffsuion;
}
public void setHeure_diffsuion(Time heure_diffsuion) {
	this.heure_diffsuion = heure_diffsuion;
}

public float getPrix() {
	return prix;
}

public void setPrix(float prix) {
	this.prix = prix;
}

public void addSeance(seance s)
{	

String ch;
driver();
OpenConnexion();
 ch="INSERT INTO `seance`(`id_seance`, `num_salle`, `date_diffusion`, `id_film`, `nbre_place`, `nbre_place_dispo`, `heure_diffusion`, `prix`) VALUES ("+s.id_seance+","+s.num_salle+",'"+s.date_diffusion+"',"+s.id_film+","+s.nbre_place+","+s.nbre_place_dispo+",'"+s.heure_diffsuion+"',"+s.prix +")";
 updateExec(ch);
 closeConnexion();
 }


public ResultSet listSeance()
{ 
	 ResultSet rs=null;
	 String ch;
	 driver();
	 OpenConnexion();
	 ch="SELECT * FROM seance";
	 rs=selectExec(ch);
	 closeConnexion();
	 return(rs);
}

public void updateSeance(int id,seance s){
	driver();
	OpenConnexion();
	updateExec("UPDATE `seance` SET `num_salle`="+s.num_salle+",`date_diffusion`='"+s.date_diffusion+"',`id_film`="+s.id_film+",`nbre_place`="+s.nbre_place+",`nbre_place_dispo`="+s.nbre_place_dispo+",`heure_diffusion`='"+s.heure_diffsuion+"' WHERE id_seance = "+id);
	closeConnexion();
	}
public void deleteSeance(int id){
	driver();
	OpenConnexion();
	updateExec("DELETE FROM `seance` WHERE id_seance = "+id);
	closeConnexion();
	}




public boolean seanceExist(seance s) throws SQLException {
	driver();
	OpenConnexion();
	String c="SELECT * FROM `seance` WHERE num_salle = "+s.getNum_salle()+" AND date_diffusion = '"+s.getDate_diffusion()+"' AND (( heure_diffusion <='"+s.getHeure_diffsuion() +"' AND heure_diffusion + INTERVAL 2 HOUR >='"+s.getHeure_diffsuion()+"' ) OR ( heure_diffusion <='"+s.getHeure_diffsuion()+"'  + INTERVAL 2 HOUR AND heure_diffusion+ INTERVAL 2 HOUR >= '"+s.getHeure_diffsuion()+"' + INTERVAL 2 HOUR ))";
	rs=selectExec(c);

	if (rs.next()) return true;

	return false;	
	
	
}

public boolean seanceExistupdate(seance s,int id) throws SQLException {
	driver();
	OpenConnexion();
	String c="SELECT * FROM `seance` WHERE num_salle = "+s.getNum_salle()+" AND date_diffusion = '"+s.getDate_diffusion()+"' AND (( heure_diffusion <='"+s.getHeure_diffsuion() +"' AND heure_diffusion + INTERVAL 2 HOUR >='"+s.getHeure_diffsuion()+"' ) OR ( heure_diffusion <='"+s.getHeure_diffsuion()+"'  + INTERVAL 2 HOUR AND heure_diffusion+ INTERVAL 2 HOUR >= '"+s.getHeure_diffsuion()+"' + INTERVAL 2 HOUR )) AND id_seance <>" + id;
	rs=selectExec(c);

	if (rs.next()) return true;

	return false;	
	
	
}


}

