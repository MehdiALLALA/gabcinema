package Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ResourceBundle;

import Model.BDcinema;
import Model.seance;
import Model.seanceDetails;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class AjoutSeanceController extends BDcinema implements Initializable {

	

	@FXML
	private Spinner <String> h_spinner;
	@FXML
	private Spinner <String> m_spinner;
	@FXML
	private ComboBox <String> cmbfilm;
	@FXML
	private ComboBox<Integer> salle;
	@FXML
	private DatePicker date;
	@FXML
	private TextField place;
	@FXML
	private TextField prix;
	@FXML
	private ImageView wallpaper;
	
	
	private int current_id;
	
	

    ObservableList<String> minutes = FXCollections.observableArrayList(//
            "00", "01", "02", "03", //
            "04", "05", "06", "07", //
            "08", "09", "10", "11","12","13","14","15","16",
            "17","18","19","20","21","22","23","24","25","26",
            "27","28","29","30","31","32","33","34","35","36",
            "37","38","39","40","41","42","43","44","45","46",
            "47","48","49","50","51","52","53","54","55","56",
            "57","58","59");
    ObservableList<String> hours = FXCollections.observableArrayList(//
            "00", "01", "02", "03", //
            "04", "05", "06", "07", //
            "08", "09", "10", "11","12","13","14","15","16",
            "17","18","19","20","21","22","23");
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	       SpinnerValueFactory<String> valueFactory1 = //
	               new SpinnerValueFactory.ListSpinnerValueFactory<String>(minutes);
	       m_spinner.setValueFactory(valueFactory1);
	       SpinnerValueFactory<String> valueFactory2 = //
	               new SpinnerValueFactory.ListSpinnerValueFactory<String>(hours);
	       h_spinner.setValueFactory(valueFactory2);
	       LoadDataFromDatabase();

		
	}
	public void LoadDataFromDatabase() {
		
		for (int i = 1; i < 11; i++) {
			salle.getItems().add(i);
			
		}
		
		driver();
		OpenConnexion();
		ResultSet rs = selectExec("SELECT id_film, titre FROM film");
		
			  try {
				  
				while(rs.next())
				  {	
				
					cmbfilm.getItems().add(rs.getInt(1)+" ["+rs.getString(2)+"]");
			
				  }
				
			} catch (SQLException e) {
				System.out.println("mafama chay");
				
				e.printStackTrace();
				
			}
	}
	public void AjoutSeance(ActionEvent event) throws IOException, SQLException {
		seance s = null;
		if(salle.getSelectionModel().getSelectedItem()==null || prix.getText().isEmpty() || place.getText().isEmpty() || date.getValue()==null || cmbfilm.getSelectionModel().getSelectedItem()==null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("tous les champs sont obligatoires");
			alert.initOwner(salle.getScene().getWindow());
			alert.showAndWait();
		}
		else
			 s = new seance();
			 s.setNum_salle(salle.getSelectionModel().getSelectedItem());
			 s.setNbre_place(Integer.parseInt(place.getText()));
			 s.setNbre_place_dispo(Integer.parseInt(place.getText()));
			 s.setDate_diffusion(Date.valueOf(date.getValue()));
			 s.setHeure_diffsuion(Time.valueOf(LocalTime.parse(h_spinner.getValue()+":"+m_spinner.getValue()+":00")));
			 s.setPrix(Float.parseFloat(prix.getText()));
			 String str = cmbfilm.getSelectionModel().getSelectedItem();
			 String [] tstr = str.split("\\s");
			 s.setId_film(Integer.parseInt(tstr[0]));
			 if(s.seanceExist(s)) {	Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				
				alert.setHeaderText("Salle occupee");
				 alert.showAndWait();
				}
			 else {
			 s.addSeance(s);
			 	Stage primaryStage =new Stage();
				File fl =new File("pictures/popcorn.jpg");
				primaryStage.getIcons().add(new Image(fl.toURI().toString()));
				Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_seance.fxml"));
				Scene scene = new Scene(root);
		     	primaryStage.setScene(scene);
		     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
		     	primaryStage.show();
				primaryStage.setTitle("MOVIEHOUSE");
				Node source = (Node) event.getSource();
			    final Stage stage = (Stage) source.getScene().getWindow();
			    stage.close();
			 }
	}
	public void back(ActionEvent e) throws IOException
	{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_seance.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		
		
	}
	public void getSeanceInfo(seanceDetails s) {
		salle.getSelectionModel().select(new Integer(s.getNum_salle()));;
		place.setText(s.getNbre_place()+"");
		prix.setText(s.getPrix()+"");
		date.setValue(s.getDate_diffusion().toLocalDate());
		String str = s.getHeure_diffusion().toString();
		String [] tstr = str.split(":");
		h_spinner.getValueFactory().setValue(tstr[0]);
		m_spinner.getValueFactory().setValue(tstr[1]);
		cmbfilm.getSelectionModel().select(s.getId_film()+" ["+s.getTitre()+"]");
		current_id = s.getId_seance();
		
	}
	
	public void getWallpaper(File file) {
		wallpaper.setImage(new Image(file.toURI().toString()));
	}
	
	public void ModifSeance(ActionEvent event) throws IOException, NumberFormatException, SQLException {
		seance s = null;
		if(salle.getSelectionModel().getSelectedItem()==null || prix.getText().isEmpty() || place.getText().isEmpty() || date.getValue()==null || cmbfilm.getSelectionModel().getSelectedItem()==null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("tous les champs sont obligatoires");
			alert.initOwner(salle.getScene().getWindow());
			alert.showAndWait();
		}
		else {
			 s = new seance();
			 s.setNum_salle(salle.getSelectionModel().getSelectedItem());
			 s.setNbre_place(Integer.parseInt(place.getText()));
			 s.setNbre_place_dispo(Integer.parseInt(place.getText()));
			 s.setDate_diffusion(Date.valueOf(date.getValue()));
			 s.setHeure_diffsuion(Time.valueOf(LocalTime.parse(h_spinner.getValue()+":"+m_spinner.getValue()+":00")));
			 s.setPrix(Float.parseFloat(prix.getText()));
			 String str = cmbfilm.getSelectionModel().getSelectedItem();
			 String [] tstr = str.split("\\s");
			 s.setId_film(Integer.parseInt(tstr[0]));
			 if(s.seanceExistupdate(s,current_id)) {	Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				
				alert.setHeaderText("Salle occupee");
				 alert.showAndWait();
				}
			 else {
			 s.updateSeance(current_id, s);
			 	Stage primaryStage =new Stage();
				File fl =new File("pictures/popcorn.jpg");
				primaryStage.getIcons().add(new Image(fl.toURI().toString()));
				Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_seance.fxml"));
				Scene scene = new Scene(root);
		     	primaryStage.setScene(scene);
			    //primaryStage.setMaximized(true);
		     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
		     	primaryStage.show();
				primaryStage.setTitle("MOVIEHOUSE");
				Node source = (Node) event.getSource();
			    final Stage stage = (Stage) source.getScene().getWindow();
			    stage.close();
			 }	
			 }
			 }
}
