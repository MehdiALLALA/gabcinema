package Controller;

import java.io.File;
import java.io.IOException;
import Model.controleur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class ajout_controleur {
	

@FXML
private TextField username;
@FXML
private TextField password;
@FXML
private Button confirmer;
@FXML
private ImageView wallpaper;

private int current_id;


	
	public void ajout(ActionEvent e) throws IOException
	{	if(password.getText().isEmpty() || username.getText().isEmpty()) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("tous les champs sont obligatoires");
		//alert.initOwner(confirmer.getScene().getWindow());
		alert.showAndWait();
	}
	else {
		controleur c = new controleur(0,username.getText(),password.getText());
		
		c.addContoleur(c);
		
		//return to film page//
		
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_controleur.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
	    // 	primaryStage.getIcons().add(new Image(this.getClass().getResource("popcorn.jpg").toString()));
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	     	primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();}
		    /////
	}
	
	public void modifier(ActionEvent e) throws IOException
	{
			
			if(password.getText().isEmpty() || username.getText().isEmpty()) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("tous les champs sont obligatoires");
				//alert.initOwner(confirmer.getScene().getWindow());
				alert.showAndWait();
			}
			else {
			controleur c = new controleur(0,username.getText(),password.getText());
			c.updateContoleur(current_id, c);
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_controleur.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
	    // 	primaryStage.getIcons().add(new Image(this.getClass().getResource("popcorn.jpg").toString()));
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	     	primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		    /////
			}
	}
	
	public void getControleurInfo(controleur c) {
		username.setText(c.getUsername());
		password.setText(c.getPassword());
		current_id = c.getId_contoleur();
	}
	public void getWallpaper(File file) {
		wallpaper.setImage(new Image(file.toURI().toString()));
	}
	
	

	
	

	
	public void back(ActionEvent e) throws IOException
	{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		   	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_controleur.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		  //  primaryStage.setMaximized(true);
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	     	primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		
		
	}
	
}


