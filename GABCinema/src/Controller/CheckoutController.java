package Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.google.zxing.WriterException;

import Model.BDcinema;
import Model.OfferDetails;
import javafx.animation.PauseTransition;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import Model.QRCodeGenerator;


public class CheckoutController extends BDcinema implements Initializable {
	
	
	public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	@FXML
	private ImageView poster;
	@FXML
	private Label lbltitre;
	@FXML
	private Label lbldesc;
	@FXML
	private Label dhlbl;
	@FXML
	private Label placelbl;
	@FXML
	private Label prixlbl;
	@FXML
	private Label sallelbl;	
	@FXML
	private Label prixttlbl;	
	@FXML
	private Spinner <Integer> nbticket;
	@FXML
	private ImageView card;
	@FXML
	private ImageView coin;
	@FXML
	private PasswordField pwd;
	@FXML
	private Button okbtn;
	private int current_id;
	
	public void getOffrer(OfferDetails od) {
	current_id =od.getId_seance();
	poster.setImage(od.getImage().getImage());
	lbltitre.setText(od.getTitre());
	lbldesc.setText(od.getDescription());
	dhlbl.setText(od.getDate().toString()+" "+od.getTime().toString());
	placelbl.setText(od.getPlace()+"");
	sallelbl.setText(od.getSalle()+"");
	prixttlbl.setText(od.getPrix()+" DT");
	prixlbl.setText(od.getPrix()+"");
    SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, od.getPlace(), 1);
    nbticket.setValueFactory(valueFactory);
    nbticket.valueProperty().addListener(new javafx.beans.value.ChangeListener<Integer>() {

		@Override
		public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
			prixttlbl.setText(Float.parseFloat(prixlbl.getText())*newValue+" DT");	
			
		}
	});


	
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		File f1 = new File("pictures/insert-coin.png");
		coin.setImage(new Image(f1.toURI().toString()));
		File f2 = new File("pictures/insert-card.jpg");
		card.setImage(new Image(f2.toURI().toString()));
		
		
		
	}
	public void Payment(ActionEvent e) {
		card.setVisible(true);
		coin.setVisible(true);
		nbticket.setDisable(true);
		
	}
	public void Annulation(ActionEvent e) {
		card.setVisible(false);
		coin.setVisible(false);
		okbtn.setVisible(false);
		pwd.setText("");
		pwd.setVisible(false);
		nbticket.setDisable(false);
		
	}
	public void CoinPayment(MouseEvent e) throws IOException {
		 driver();
		 OpenConnexion();
		 updateExec("UPDATE `seance` SET `nbre_place_dispo`="+(Integer.parseInt(placelbl.getText())-nbticket.getValue())+" WHERE id_seance ="+this.current_id);
		 for(int i=0;i<nbticket.getValue();i++) {
		 String key = getAlphaNumericString(8);
		 String ticket = key+"|"+lbltitre.getText()+"|"+dhlbl.getText()+"|"+sallelbl.getText()+"|"+prixlbl.getText();
		 updateExec("INSERT INTO `ticket` (`qrcode`) VALUES ('"+ticket+"')");
		 try {
			QRCodeGenerator.generateQRCodeImage(ticket, 350, 350, "tickets/"+key+".png");
		} catch (WriterException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 }
		 Stage primaryStage =new Stage();
		  File f =new File("pictures/popcorn.jpg");
		  primaryStage.getIcons().add(new Image(f.toURI().toString()));
		  Parent root=FXMLLoader.load(getClass().getResource("/View/Ticket.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		 //   primaryStage.setMaximized(true);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
			PauseTransition delay = new PauseTransition(Duration.seconds(10));
			delay.setOnFinished( eventclosing -> {primaryStage.close();
			Stage secondStage =new Stage();
			try {
			  Parent secondroot=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
				Scene secondscene = new Scene(secondroot);
				secondStage.setScene(secondscene);
				secondStage.show();
				secondStage.setTitle("MOVIEHOUSE");
				File fl =new File("pictures/popcorn.jpg");
				secondStage.resizableProperty().setValue(Boolean.FALSE);
				secondStage.getIcons().add(new Image(fl.toURI().toString()));
				secondscene.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
			
						if(kb.match(event))
						{	   
							Scene s = (Scene) event.getSource();
							Node nodeToFind = secondscene.lookup("#adminbtn");
							nodeToFind.setVisible(true);
						}
							
								
					}
					
				});

			}
			catch(IOException ex) {
				ex.printStackTrace();
			}
				
			} );
			delay.play();
	}
	
	public void back(ActionEvent e) throws IOException{
	 	Stage primaryStage =new Stage();
	 	File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
		Parent root=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
		Scene scene = new Scene(root);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
	
				if(kb.match(event))
				{	   
					Scene s = (Scene) event.getSource();
					Node nodeToFind = scene.lookup("#adminbtn");
					nodeToFind.setVisible(true);
				}
					
						
			}
			
		});
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
  }	
	public void DisplayCode(MouseEvent event) {
		pwd.setText("");
		pwd.setVisible(true);
		okbtn.setVisible(true);	
	}
	public void CardPayment(ActionEvent e) throws IOException {
		if(pwd.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez saisir le code de votre carte");
			alert.showAndWait();
			
		}
		else {
			 driver();
			 OpenConnexion();
			 updateExec("UPDATE `seance` SET `nbre_place_dispo`="+(Integer.parseInt(placelbl.getText())-nbticket.getValue())+" WHERE id_seance ="+this.current_id);
			 for(int i=0;i<nbticket.getValue();i++) {
			 String key = getAlphaNumericString(8);
			 String ticket = key+"|"+lbltitre.getText()+"|"+dhlbl.getText()+"|"+sallelbl.getText()+"|"+prixlbl.getText();
			 updateExec("INSERT INTO `ticket` (`qrcode`) VALUES ('"+ticket+"')");
			 try {
				QRCodeGenerator.generateQRCodeImage(ticket, 350, 350, "tickets/"+key+".png");
			} catch (WriterException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 }
			 Stage primaryStage =new Stage();
			  File f =new File("pictures/popcorn.jpg");
			  primaryStage.getIcons().add(new Image(f.toURI().toString()));
			  Parent root=FXMLLoader.load(getClass().getResource("/View/Ticket.fxml"));
				Scene scene = new Scene(root);
		     	primaryStage.setScene(scene);
			 //   primaryStage.setMaximized(true);
				primaryStage.show();
				primaryStage.resizableProperty().setValue(Boolean.FALSE);
				primaryStage.setTitle("MOVIEHOUSE");
				Node source = (Node) e.getSource();
			    final Stage stage = (Stage) source.getScene().getWindow();
			    stage.close();
				PauseTransition delay = new PauseTransition(Duration.seconds(10));
				delay.setOnFinished( eventclosing -> {primaryStage.close();
				Stage secondStage =new Stage();
				try {
				  Parent secondroot=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
					Scene secondscene = new Scene(secondroot);
					secondStage.setScene(secondscene);
					secondStage.show();
					secondStage.setTitle("MOVIEHOUSE");
					File fl =new File("pictures/popcorn.jpg");
					secondStage.resizableProperty().setValue(Boolean.FALSE);
					secondStage.getIcons().add(new Image(fl.toURI().toString()));
					secondscene.setOnKeyPressed(new EventHandler<KeyEvent>() {

						@Override
						public void handle(KeyEvent event) {
				
							if(kb.match(event))
							{	   
								Scene s = (Scene) event.getSource();
								Node nodeToFind = secondscene.lookup("#adminbtn");
								nodeToFind.setVisible(true);
							}
								
									
						}
						
					});

				}
				catch(IOException ex) {
					ex.printStackTrace();
				}
					
				} );
				delay.play();
		
		}
	}
}
