package Controller;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainController {
	  
	public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;
	
	@FXML
	private AnchorPane ap;
	
	  
	public void Viewoffers(MouseEvent e) throws IOException{
			    Stage primaryStage =new Stage();
				File f =new File("pictures/popcorn.jpg");
				primaryStage.getIcons().add(new Image(f.toURI().toString()));
			    Parent root=FXMLLoader.load(getClass().getResource("/View/Offers.fxml"));
				Scene scene = new Scene(root);
				primaryStage.setScene(scene);
				primaryStage.show();
				primaryStage.setTitle("MOVIEHOUSE");
				primaryStage.resizableProperty().setValue(Boolean.FALSE);
				Node source = (Node) e.getSource();
			    final Stage stage = (Stage) source.getScene().getWindow();
			    stage.close();
		  }
	
	public void Viewlogin(MouseEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/login_page.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }
	@FXML
	public void ViewManager(KeyEvent e) throws IOException {
		if(ap.isFocused())
			System.out.println("aaaaa");
		
	}
	
	
	
 }

