package Controller;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import Model.BDcinema;
import Model.QRCodeReader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class VerifTicketController extends BDcinema{
	public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;
	
	@FXML
	private Label scanlbl;
	public void back(ActionEvent e) throws IOException{
	 	Stage primaryStage =new Stage();
		File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
	 	Parent root=FXMLLoader.load(getClass().getResource("/View/personnel_page.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
  }	
	public void back1(ActionEvent e) throws IOException{
		Stage primaryStage =new Stage();
        Parent root=FXMLLoader.load(getClass().getResource("/View/login_page.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setTitle("MOVIEHOUSE");
		
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
	
				if(kb.match(event))
				{	   
					Scene s = (Scene) event.getSource();
					Node nodeToFind = scene.lookup("#adminbtn");
					nodeToFind.setVisible(true);
				}
					
						
			}
			
		});
  }	
	
	
	
	public void scanticket(ActionEvent event) throws SQLException {
		driver();
		OpenConnexion();
		scanlbl.setText("");
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(new File("./tickets"));
		File file = fc.showOpenDialog(null);
		if(file!=null) {
			try {
				String decodedText = QRCodeReader.decodeQRCode(file);
				ResultSet rs = selectExec("SELECT * FROM `ticket` WHERE `qrcode` = '"+decodedText+"'");
				if(rs.next()) {
					updateExec("DELETE FROM `ticket` WHERE `qrcode` = '"+decodedText+"'");
					file.delete();
					scanlbl.setTextFill(Color.DARKGREEN);
					scanlbl.setText("Ticket Verifi�");
				}
				else {
					file.delete();
					scanlbl.setTextFill(Color.DARKRED);
					scanlbl.setText("Ticket Non Verifi�");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
