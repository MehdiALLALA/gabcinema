package Controller;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import Model.BDcinema;

public class loginController {
public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;
	
	@FXML
	private TextField user;
	@FXML
	private TextField pass;
	
	public void login(ActionEvent event) throws SQLException, IOException {
		
		 String username = user.getText();
		 String password = pass.getText();
		 BDcinema dbase = new BDcinema();
		 dbase.driver();
		 dbase.OpenConnexion();
		 ResultSet rs = dbase.selectExec("SELECT * FROM personnel WHERE username='"+username+"' AND password='"+password+"'");
		 ResultSet rs1 = dbase.selectExec("SELECT * FROM controleur WHERE username='"+username+"' AND password='"+password+"'");
		if (rs.next()) {
			 Stage primaryStage =new Stage();
			  File f =new File("pictures/popcorn.jpg");
			  primaryStage.getIcons().add(new Image(f.toURI().toString()));
			  Parent root=FXMLLoader.load(getClass().getResource("/View/personnel_page.fxml"));
				Scene scene = new Scene(root);
		     	primaryStage.setScene(scene);
			 //   primaryStage.setMaximized(true);
				primaryStage.show();
				primaryStage.resizableProperty().setValue(Boolean.FALSE);
				primaryStage.setTitle("MOVIEHOUSE");
				Node source = (Node) event.getSource();
			    final Stage stage = (Stage) source.getScene().getWindow();
			    stage.close();
			
		}
		else if (rs1.next()) {		 Stage primaryStage =new Stage();
		  File f =new File("pictures/popcorn.jpg");
		  primaryStage.getIcons().add(new Image(f.toURI().toString()));
		  Parent root=FXMLLoader.load(getClass().getResource("/View/controleurpage.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		 //   primaryStage.setMaximized(true);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) event.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();}
		else {		
			Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("username et password incorrect");
		 alert.showAndWait();
		 }
		 dbase.closeResultSet();
		 dbase.closeStatement();
		 dbase.closeConnexion();
		 
	}
	public void back(ActionEvent e) throws IOException{
	 	Stage primaryStage =new Stage();
	 	File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
		Parent root=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
		Scene scene = new Scene(root);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
	
				if(kb.match(event))
				{	   
					Scene s = (Scene) event.getSource();
					Node nodeToFind = scene.lookup("#adminbtn");
					nodeToFind.setVisible(true);
				}
					
						
			}
			
		});
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
  }	

}
