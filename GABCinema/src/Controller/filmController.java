package Controller;
import Model.film;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import Model.BDcinema;
import Model.FilmDetails;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.fxml.Initializable;
public class filmController  extends BDcinema implements Initializable {
	
	@FXML
	private Button ajouter;
	@FXML
	private Button modifier;
	@FXML
	private Button supprimer;

	@FXML 
	private TableColumn<FilmDetails, String> col_titre;
	@FXML
	private TableColumn<FilmDetails, String> col_image;
	@FXML 
	private TableColumn<FilmDetails, String> col_resume;
	@FXML
	private TableColumn<FilmDetails, Date> col_date;
	@FXML
	private TableColumn<FilmDetails, String> col_imagepath;
	
	
	@FXML
	private TableView<FilmDetails> tableaufilm;
	ObservableList<FilmDetails> table;


	public void setCellTable() {
		  col_titre.setCellValueFactory(new PropertyValueFactory <>("titre"));
		  col_resume.setCellValueFactory(new PropertyValueFactory <>("resume"));
		  col_image.setCellValueFactory(new PropertyValueFactory <>("image"));
		  col_date.setCellValueFactory(new PropertyValueFactory <>("date"));
		  col_imagepath.setCellValueFactory(new PropertyValueFactory <>("image_path"));
		  	}

	public void LoadDataFromDatabase () {
		
		table = FXCollections.observableArrayList();
		driver();
		OpenConnexion();
		ResultSet rs = selectExec("SELECT * FROM `film`");

			  try {
				  
				while(rs.next())
				  {	
					File f =new File(rs.getString(4));
					ImageView image=new ImageView();
					image.setImage(new Image(f.toURI().toString()));
					image.setFitHeight(300);
					image.setFitWidth(225);
					table.add(new FilmDetails(image,rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5)));
					  
					
				 }
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}

			 tableaufilm.setItems(table);
			  autoResizeColumns(tableaufilm);
			  
			  

	}
	
	
	
	public void ajout(MouseEvent e) throws IOException{
		
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/ajout_film.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			//primaryStage.getIcons().add(new Image(this.getClass().getResource("popcorn.jpg").toString()));
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	public static void autoResizeColumns( TableView<?> table )


	{
	    //Set the right policy
	    table.setColumnResizePolicy( TableView.UNCONSTRAINED_RESIZE_POLICY);
	    table.getColumns().stream().forEach( (column) ->
	    {
	        //Minimal width = columnheader
	        Text t = new Text( column.getText() );
	        double max = t.getLayoutBounds().getWidth();
	        for ( int i = 0; i < table.getItems().size(); i++ )
	        {
	            //cell must not be empty
	            if ( column.getCellData( i ) != null )
	            {
	                t = new Text( column.getCellData( i ).toString() );
	                double calcwidth = t.getLayoutBounds().getWidth();
	                //remember new max-width
	                if ( calcwidth > max )
	                {
	                    max = calcwidth;
	                }
	            }
	        }
	        //set the new max-widht with some extra space
	        column.setPrefWidth( max + 10.0d );
	    } );
	    
	    
	    
	}
	
	public FilmDetails itemSelected() 
	{
		FilmDetails f=tableaufilm.getSelectionModel().getSelectedItem();  
	 
		return (f);
	}
	
	
	public void update(ActionEvent e) throws IOException {
		
		  
		FilmDetails f =  itemSelected();
		if(f!=null) {
		Stage primaryStage =new Stage();
		File file =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(file.toURI().toString()));
		FXMLLoader loader = new FXMLLoader();
		Parent root=loader.load(getClass().getResource("/View/update_film.fxml").openStream());
		ajoutController ac = (ajoutController) loader.getController();
		ac.getFilmInfo(f);
		File wp = new File("pictures/mainview1.jpg");
		ac.getWallpaper(wp);
		Scene scene = new Scene(root);
     	primaryStage.setScene(scene);
     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.show();
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();}
		
		else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez selectionner un film pour le modifier");
			alert.showAndWait();
		}
		
	}
	
	public void delete (ActionEvent e) throws IOException {
			
			FilmDetails f =  itemSelected();
			if(f!=null) {
			
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmation ");
				alert.setHeaderText("");
				alert.setContentText("etes vous sur?");
				Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
				File f1 =new File("pictures/popcorn.jpg");
				stage1.getIcons().add(new Image(f1.toURI().toString()));

				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK){
					String ch= f.getTitre();
					film f12 = new film();
					f12.deleteFilm(ch);
				
			} 
					Stage primaryStage =new Stage();
			File file =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(file.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		 
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
			}
			else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("Veuillez selectionner un film pour le supprimer");
				alert.showAndWait();
			}
		
	}

	public void back(ActionEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/personnel_page.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setCellTable();
		LoadDataFromDatabase();
		col_resume.setCellFactory(WRAPPING_CELL_FACTORY);
		col_titre.setCellFactory(WRAPPING_CELL_FACTORY);
		
		
		
	}
	  public static final Callback<TableColumn<FilmDetails,String>, TableCell<FilmDetails,String>> WRAPPING_CELL_FACTORY = 
	            new Callback<TableColumn<FilmDetails,String>, TableCell<FilmDetails,String>>() {
	                
	        @Override public TableCell<FilmDetails,String> call(TableColumn<FilmDetails,String> param) {
	            TableCell<FilmDetails,String> tableCell = new TableCell<FilmDetails,String>() {
	                @Override protected void updateItem(String item, boolean empty) {
	                    if (item == getItem()) return;

	                    super.updateItem(item, empty);

	                    if (item == null) {
	                        super.setText(null);
	                        super.setGraphic(null);
	                    } else {
	                        super.setText(null);
	                        Label l = new Label(item);
	                        l.setWrapText(true);
	                        VBox box = new VBox(l);
	                        l.heightProperty().addListener((observable,oldValue,newValue)-> {
	                        	box.setPrefHeight(newValue.doubleValue()+7);
	                        	Platform.runLater(()->this.getTableRow().requestLayout());
	                        });
	                        super.setGraphic(box);
	                    }
	                }
	            };
		    return tableCell;
	        }
	    };
	
	
		
	
	
	
	
	

}
