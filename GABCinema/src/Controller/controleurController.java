package Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import Model.BDcinema;
import Model.controleur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class controleurController extends BDcinema implements Initializable {
	@FXML
	private Button ajouter;
	@FXML
	private Button modifier;
	@FXML
	private Button supprimer;
	@FXML
	private TableColumn<controleur, String> col_username;
	@FXML 
	private TableColumn<controleur, String> col_password;
	@FXML
	private TableView<controleur> tableaucontroleur;
	ObservableList<controleur> table;
	
	public controleur itemSelected() 
	{
		
		
		controleur c = new controleur(0, null, null);
          c=tableaucontroleur.getSelectionModel().getSelectedItem();  
	 
		return (c);
	}
	
	public void delete (ActionEvent e) throws IOException {
		controleur c =  itemSelected();
		if(c!=null) {
	
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation ");
		alert.setHeaderText("");
		alert.setContentText("etes vous sur?");
		Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
		File f1 =new File("pictures/popcorn.jpg");
		stage1.getIcons().add(new Image(f1.toURI().toString()));

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			c.deleteontroleur(c.getId_contoleur());
	} 
		
		
		
		 Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		  Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_controleur.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		  //  primaryStage.setMaximized(true);
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
		//	primaryStage.getIcons().add(new Image(this.getClass().getResource("popcorn.jpg").toString()));
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();}
		
		else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez selectionner un controleur pour le supprimer");
			alert.showAndWait();
		}
		}

	public void setCellTable() {
	
		  col_username.setCellValueFactory(new PropertyValueFactory <>("username"));
		  col_password.setCellValueFactory(new PropertyValueFactory <>("password"));

		  	}
	
	
	public void LoadDataFromDatabase () {
		
		table = FXCollections.observableArrayList();
		driver();
		OpenConnexion();
		ResultSet rs = selectExec("SELECT * FROM `controleur`");

			  try {
				  
				while(rs.next())
				  {	
				
					table.add(new controleur(rs.getInt(1), rs.getString(2), rs.getString(3)));
					  
					
				  }
				
			} catch (SQLException e) {
				
				e.printStackTrace();
				
			}


			  tableaucontroleur.setItems(table);
			 
			 }
	
	
	public void ajout(ActionEvent e) throws IOException{
	

		
		 Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		  Parent root=FXMLLoader.load(getClass().getResource("/View/ajout_controleur.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	
	public void modifier(ActionEvent e) throws IOException{
		

			controleur c=tableaucontroleur.getSelectionModel().getSelectedItem();  
			
			if(c!=null) {
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			FXMLLoader loader = new FXMLLoader();
			Parent root=loader.load(getClass().getResource("/View/update_controleur.fxml").openStream());
			ajout_controleur ac = (ajout_controleur) loader.getController();
			ac.getControleurInfo(c);
			File wp = new File("pictures/mainview1.jpg");
			ac.getWallpaper(wp);
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();}
			
			else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("Veuillez selectionner un controleur pour le modifier");
				alert.showAndWait();
			}
	  }	
	
	
	
	public void back(ActionEvent e) throws IOException{
		 Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		  Parent root=FXMLLoader.load(getClass().getResource("/View/personnel_page.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setCellTable();
		LoadDataFromDatabase();
	}

}
