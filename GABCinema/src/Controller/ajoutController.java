package Controller;
import Model.BDcinema;
import Model.FilmDetails;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import Model.film;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public class ajoutController {
	
	
//////ajouter film///////
@FXML
private TextField nom;
@FXML
private TextField resume;
@FXML
private DatePicker date;
@FXML
private TextField image;
@FXML
private Button confirmer;
@FXML
private Button choisirimage;
@FXML
private ImageView imageview;
@FXML
private ImageView wallpaper;

private int current_id;

private String link = null;

private String oldlink = null;

private boolean uploaded = false;

	
	public void ajoutFilm(MouseEvent e) throws IOException, SQLException
	{if (nom.getText().trim().isEmpty() ||resume.getText().trim().isEmpty() || date.toString()==null || link ==null ) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
	
		alert.setHeaderText("tous les champs sont obligatoires");
		 alert.showAndWait();
	}
	
	
	else {
	
		film f=new film();
		if(f.filmExist(nom.getText())) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("le film existe deja");
			 alert.showAndWait();
			
			
		}
		else {
		f.setTitre(nom.getText());
		f.setId_film(0);
	    Date date1 = Date.valueOf((date.getValue()));
		f.setDate(date1);
		f.setResume(resume.getText());
		f.setImage(link);
		f.addFilm(f);
		
		//return to film page//
		
		 	Stage primaryStage =new Stage();
			File fl =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(fl.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
	     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	     	primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		    
		}}}
	
	
	public void ajouterImage(ActionEvent e) throws IOException
	{
		FileChooser fc = new FileChooser();
		FileChooser fc1 = new FileChooser();
		
		File file = fc.showOpenDialog(null) ;
		BufferedImage img = ImageIO.read(file);
		Image pic = new Image(file.toURI().toString());
		if (pic!= null ) {
			image.setText(file.getAbsolutePath().toString());
			imageview.setImage(pic);
			link="pictures/"+BDcinema.getAlphaNumericString(10)+".jpg";
			ImageIO.write(img,"jpg",new File(link));
			uploaded = true;
		}
		
		
	}
	public void back_insert(ActionEvent e) throws IOException
	{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		
		
	}
	public void back(ActionEvent e) throws IOException
	{
			if(!oldlink.equals(link)) {
				File fl = new File(link);
				fl.delete();
			}
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
		 	Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		
		
	}
	public void getFilmInfo(FilmDetails f) {
		nom.setText(f.getTitre());
		resume.setText(f.getResume());
		date.setValue(f.getDate().toLocalDate());
		image.setText(f.getImage_path());
		oldlink = f.getImage_path();
		link = f.getImage_path();
		imageview.setImage(f.getImage().getImage());
		current_id = f.getId_film();
	}
	public void getWallpaper(File file) {
		wallpaper.setImage(new Image(file.toURI().toString()));
	}
	
	public void ModifierFilm(ActionEvent event) throws IOException, SQLException {
		if (nom.getText().isEmpty() ||resume.getText().isEmpty() || date.getValue()==null || link ==null ) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			
			alert.setHeaderText("tous les champs sont obligatoires");
			alert.showAndWait();
		}
		else {
			film f = new film();
			f.setTitre(nom.getText());
		    Date date1 = Date.valueOf((date.getValue()));
			f.setDate(date1);
			f.setResume(resume.getText());
		
			if(!uploaded) {
				f.setImage(oldlink);
			}
			else {
					
				f.setImage(link);
				File fl = new File(oldlink);
				fl.delete();
			}
			
			
			if(f.filmExist(nom.getText(),current_id)) {
				 Alert alert = new Alert(AlertType.WARNING);
				 alert.setTitle("Warning");
			     alert.setHeaderText("le film existe deja");
				 alert.showAndWait();
				
				
			}
			else {
			f.updateFilm(current_id, f);
			
		 	Stage primaryStage =new Stage();
			File fl =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(fl.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
	     	primaryStage.setScene(scene);
		   	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	     	primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) event.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
			}
		}
	}
}
