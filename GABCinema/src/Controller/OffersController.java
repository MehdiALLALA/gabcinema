package Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ResourceBundle;
import Model.OfferDetails;
import Model.BDcinema;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;


public class OffersController extends BDcinema implements Initializable{
	@FXML 
	private TableColumn<OfferDetails, String> col_titre;
	@FXML
	private TableColumn<OfferDetails, String> col_poster;
	@FXML 
	private TableColumn<OfferDetails, String> col_description;
	@FXML
	private TableColumn<OfferDetails, Date> col_date;
	@FXML
	private TableColumn<OfferDetails, Time> col_heure;
	@FXML
	private TableColumn<OfferDetails, ?> col_salle;
	@FXML
	private TableColumn<OfferDetails, ?> col_prix;
	@FXML
	private TableColumn<OfferDetails, ?> col_place;
public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;
	
	
	@FXML
	private TableView<OfferDetails> tableauoffers;
	ObservableList<OfferDetails> table;
	
	@FXML
	private Button btnbuy;
	
	public void setCellTable() {
		  col_titre.setCellValueFactory(new PropertyValueFactory <>("titre"));
		  col_description.setCellValueFactory(new PropertyValueFactory <>("description"));
		  col_poster.setCellValueFactory(new PropertyValueFactory <>("image"));
		  col_date.setCellValueFactory(new PropertyValueFactory <>("date"));
		  col_heure.setCellValueFactory(new PropertyValueFactory <>("time"));
		  col_salle.setCellValueFactory(new PropertyValueFactory <>("salle"));
		  col_prix.setCellValueFactory(new PropertyValueFactory <>("prix"));
		  col_place.setCellValueFactory(new PropertyValueFactory <>("place"));
		  
		  	}
	
public void LoadDataFromDatabase () {
		
		table = FXCollections.observableArrayList();
		driver();
		OpenConnexion();
		ResultSet rs = selectExec("SELECT film.image,film.titre, film.resume, seance.date_diffusion, seance.heure_diffusion, seance.num_salle, seance.prix, seance.nbre_place_dispo, id_seance FROM film, seance WHERE film.id_film=seance.id_film AND (seance.date_diffusion>CURRENT_DATE OR (seance.date_diffusion=CURRENT_DATE AND seance.heure_diffusion>CURRENT_TIME))");

			  try {
				  
				while(rs.next())
				  {	
					File f =new File(rs.getString(1));
					ImageView image=new ImageView();
					image.setImage(new Image(f.toURI().toString()));
					image.setFitHeight(300);
					image.setFitWidth(225);
					table.add(new OfferDetails(image,rs.getInt(9),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getTime(5),rs.getInt(6),rs.getFloat(7),rs.getInt(8)));
					  
					
				  }
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}


			  tableauoffers.setItems(table);
		
}

	

public void back(ActionEvent e) throws IOException{
 	Stage primaryStage =new Stage();
 	File f =new File("pictures/popcorn.jpg");
	primaryStage.getIcons().add(new Image(f.toURI().toString()));
	Parent root=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
	Scene scene = new Scene(root);
	scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

		@Override
		public void handle(KeyEvent event) {

			if(kb.match(event))
			{	   
				Scene s = (Scene) event.getSource();
				Node nodeToFind = scene.lookup("#adminbtn");
				nodeToFind.setVisible(true);
			}
				
					
		}
		
	});
	primaryStage.setScene(scene);
	primaryStage.show();
	primaryStage.resizableProperty().setValue(Boolean.FALSE);
	primaryStage.setTitle("MOVIEHOUSE");
	Node source = (Node) e.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
}	





@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setCellTable();
		LoadDataFromDatabase();
		col_description.setCellFactory(WRAPPING_CELL_FACTORY);
		col_titre.setCellFactory(WRAPPING_CELL_FACTORY);
		
	}
    public static final Callback<TableColumn<OfferDetails,String>, TableCell<OfferDetails,String>> WRAPPING_CELL_FACTORY = 
            new Callback<TableColumn<OfferDetails,String>, TableCell<OfferDetails,String>>() {
                
        @Override public TableCell<OfferDetails,String> call(TableColumn<OfferDetails,String> param) {
            TableCell<OfferDetails,String> tableCell = new TableCell<OfferDetails,String>() {
                @Override protected void updateItem(String item, boolean empty) {
                    if (item == getItem()) return;

                    super.updateItem(item, empty);

                    if (item == null) {
                        super.setText(null);
                        super.setGraphic(null);
                    } else {
                        super.setText(null);
                        Label l = new Label(item);
                        l.setWrapText(true);
                        VBox box = new VBox(l);
                        l.heightProperty().addListener((observable,oldValue,newValue)-> {
                        	box.setPrefHeight(newValue.doubleValue()+7);
                        	Platform.runLater(()->this.getTableRow().requestLayout());
                        });
                        super.setGraphic(box);
                    }
                }
            };
	    return tableCell;
        }
    };
	
	public void OrderCheckout(ActionEvent e) {
		OfferDetails od = tableauoffers.getSelectionModel().getSelectedItem();
		if(od==null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez selectionner une offre");
			alert.showAndWait();
		}
		else {
    	    try {
    	        Stage primaryStage =new Stage();
    		 	File f =new File("pictures/popcorn.jpg");
    			primaryStage.getIcons().add(new Image(f.toURI().toString()));
    	        FXMLLoader loader = new FXMLLoader();
    		    Parent root= loader.load(getClass().getResource("/View/Checkout.fxml").openStream());
    		    CheckoutController checkoutcontroller = (CheckoutController) loader.getController();
    		    checkoutcontroller.getOffrer(od);
    			Scene scene = new Scene(root);
    			primaryStage.setScene(scene);
    			primaryStage.show();
    			primaryStage.resizableProperty().setValue(Boolean.FALSE);
    			primaryStage.setTitle("MOVIEHOUSE");
    			Stage stage = (Stage) btnbuy.getScene().getWindow();
    			stage.close();
    	    }
    	    catch(IOException ex) {
    	    	ex.printStackTrace();
    	    }
		}
	}
}

