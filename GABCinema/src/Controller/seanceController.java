package Controller;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import Model.BDcinema;
import Model.seance;
import Model.seanceDetails;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class seanceController extends BDcinema implements Initializable {
	@FXML
	private Button ajouter;
	@FXML
	private Button modifier;
	@FXML
	private Button supprimer;

	@FXML 
	private TableColumn<seanceDetails, String> col_id_seance;
	@FXML
	private TableColumn<seanceDetails, String> col_num_salle;
	@FXML 
	private TableColumn<seanceDetails, Date> col_date_diffusion;
	@FXML
	private TableColumn<seanceDetails, String> col_id_film;
	@FXML
	private TableColumn<seanceDetails, String> col_nbre_place;
	@FXML
	private TableColumn<seanceDetails, String> col_nbre_place_dispo;
	@FXML
	private TableColumn<seanceDetails, String> col_heure_diffusion;
	@FXML
	private TableColumn<seanceDetails, String> col_prix;
	@FXML
	private TableColumn<seanceDetails, String> col_titre;
	@FXML
	private TableView<seanceDetails> tableauseance;
	ObservableList<seanceDetails> table;
	
	public void setCellTable() {
		  col_id_seance.setCellValueFactory(new PropertyValueFactory <>("id_seance"));
		  col_num_salle.setCellValueFactory(new PropertyValueFactory <>("num_salle"));
		  col_date_diffusion.setCellValueFactory(new PropertyValueFactory <>("date_diffusion"));
		  col_id_film.setCellValueFactory(new PropertyValueFactory <>("id_film"));
		  col_nbre_place.setCellValueFactory(new PropertyValueFactory <>("nbre_place"));
		  col_nbre_place_dispo.setCellValueFactory(new PropertyValueFactory <>("nbre_place_dispo"));
		  col_heure_diffusion.setCellValueFactory(new PropertyValueFactory <>("heure_diffusion"));
		  col_prix.setCellValueFactory(new PropertyValueFactory <>("prix"));
		  col_titre.setCellValueFactory(new PropertyValueFactory <>("titre"));
		  	}
	public static void autoResizeColumns( TableView<?> table )


	{
	    //Set the right policy
	    table.setColumnResizePolicy( TableView.UNCONSTRAINED_RESIZE_POLICY);
	    table.getColumns().stream().forEach( (column) ->
	    {
	        //Minimal width = columnheader
	        Text t = new Text( column.getText() );
	        double max = t.getLayoutBounds().getWidth();
	        for ( int i = 0; i < table.getItems().size(); i++ )
	        {
	            //cell must not be empty
	            if ( column.getCellData( i ) != null )
	            {
	                t = new Text( column.getCellData( i ).toString() );
	                double calcwidth = t.getLayoutBounds().getWidth();
	                //remember new max-width
	                if ( calcwidth > max )
	                {
	                    max = calcwidth;
	                }
	            }
	        }
	        //set the new max-widht with some extra space
	        column.setPrefWidth( max + 10.0d );
	    } );
	    
	    
	    
	    
	}
	
	public ResultSet filmTitre(int id) throws SQLException
	{
		
		String ch="SELECT * FROM `film` WHERE `id_film` = " + id ;
		ResultSet rs = selectExec(ch);
		return(rs);
		
	}
	
	public void LoadDataFromDatabase () {
		
		table = FXCollections.observableArrayList();
		driver();
		OpenConnexion();
		ResultSet rs = selectExec("SELECT id_seance, num_salle, date_diffusion, s.id_film, f.titre, nbre_place, nbre_place_dispo, heure_diffusion, prix FROM seance s, film f WHERE s.id_film = f.id_film");
		
			  try {
				  
				while(rs.next())
				  {	
				
					
			table.add( new seanceDetails(rs.getInt("id_seance"), rs.getInt("num_salle"), rs.getInt("id_film"), rs.getInt("nbre_place"), rs.getInt("nbre_place_dispo"),rs.getString(5), rs.getDate("date_diffusion"), rs.getTime("heure_diffusion").toString(),rs.getFloat("prix")));
			
				  }
				
			} catch (SQLException e) {
			
				e.printStackTrace();
				
			}
			  
			  
			  


			  tableauseance.setItems(table);
			  autoResizeColumns(tableauseance);
			  
			  

	}
	
	
	public void delete (ActionEvent e) throws IOException {
		seanceDetails d=tableauseance.getSelectionModel().getSelectedItem();  
		
		if(d!=null) {
		
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation ");
			alert.setHeaderText("");
			alert.setContentText("etes vous sur?");
			Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
			File f1 =new File("pictures/popcorn.jpg");
			stage1.getIcons().add(new Image(f1.toURI().toString()));

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK){
				seance s = new seance(0, 0, 0, 0, 0, null, null, 0);
				s.deleteSeance(d.getId_seance());
				
		} 
			
			
			
			
	
	 	
		
		
		Stage primaryStage =new Stage();
		File file =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(file.toURI().toString()));
		Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_seance.fxml"));
		Scene scene = new Scene(root);
     	primaryStage.setScene(scene);
	 
     	primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.show();
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
		}
		else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez selectionner une seance pour la supprimer");
			alert.showAndWait();
		}
	
}
	
	
	
	public void back(ActionEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/personnel_page.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	
	public void add(ActionEvent e) throws IOException {
	 	Stage primaryStage =new Stage();
		File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
		Parent root=FXMLLoader.load(getClass().getResource("/View/AjoutSeance.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
	}
	
	public void update(ActionEvent e) throws IOException {
	 	seanceDetails s=tableauseance.getSelectionModel().getSelectedItem();
	 	
	 	if (s!=null) {
		Stage primaryStage =new Stage();
		File f =new File("pictures/popcorn.jpg");
		primaryStage.getIcons().add(new Image(f.toURI().toString()));
		FXMLLoader loader = new FXMLLoader();
		Parent root=loader.load(getClass().getResource("/View/update_seance.fxml").openStream());
		AjoutSeanceController ac = (AjoutSeanceController) loader.getController();
		ac.getSeanceInfo(s);
		File wp = new File("pictures/mainview1.jpg");
		ac.getWallpaper(wp);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.resizableProperty().setValue(Boolean.FALSE);
		primaryStage.setTitle("MOVIEHOUSE");
		Node source = (Node) e.getSource();
	    final Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();}
		else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Veuillez selectionner une seance pour la modifier");
			alert.showAndWait();
		}
	}
	
	
	
	
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		setCellTable();
		LoadDataFromDatabase();
	}
	
	
	
	
	

}
