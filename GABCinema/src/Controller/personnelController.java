package Controller;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class personnelController {
public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;
	@FXML
	private Button consulterFilm;
	@FXML
	private Button consulterSeance;
	@FXML
	private Button consulterController;
	@FXML
	private Button verifierTicket;
	
	
	public void film(MouseEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
		 	File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_film.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			//primaryStage.getIcons().add(new Image(this.getClass().getResource("popcorn.jpg").toString()));
			
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	public void seance(MouseEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_seance.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	
	public void controleur(MouseEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/consulter_controleur.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	public void ticket(MouseEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/verifier_ticket.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	

	
	public void back(ActionEvent e) throws IOException{
		 	Stage primaryStage =new Stage();
		 	File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			Parent root=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
			Scene scene = new Scene(root);
			scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

				@Override
				public void handle(KeyEvent event) {
		
					if(kb.match(event))
					{	   
						Scene s = (Scene) event.getSource();
						Node nodeToFind = scene.lookup("#adminbtn");
						nodeToFind.setVisible(true);
					}
						
							
				}
				
			});
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			primaryStage.setTitle("MOVIEHOUSE");
			Node source = (Node) e.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
	  }	
	

}
