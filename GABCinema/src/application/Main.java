package application;
	


import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;



public class Main extends Application{
	
	public static final KeyCombination kb = new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN);
	
	@FXML
	private Button adminbtn;

	@Override
	public void start(Stage primaryStage) throws IOException {
	

            Parent root=FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("MOVIEHOUSE");
			
			primaryStage.resizableProperty().setValue(Boolean.FALSE);
			File f =new File("pictures/popcorn.jpg");
			primaryStage.getIcons().add(new Image(f.toURI().toString()));
			scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

				@Override
				public void handle(KeyEvent event) {
		
					if(kb.match(event))
					{	   
						Scene s = (Scene) event.getSource();
						Node nodeToFind = scene.lookup("#adminbtn");
						nodeToFind.setVisible(true);
					}
						
							
				}
				
			});
	    

	}
	
	public static void main(String[] args) {
		launch(args);
	}




}